import React from "react"
import "./EstateCardView.css"

let EstateCardView = (props) => {
    return (
            <div className="card"
                 onClick={() => props.estateClick(props.link)}
                 data-toggle="modal"
                 data-target="#myModal">
                <img
                    src={props.imageLink}
                    className="card-img-top"
                    alt={"imageEstate"}/>
                <span className={"card-img-overlay"}>
                    <span className={"btn"}>!</span>
                    <h4 className={"optionsHeading"}>Options</h4>
                    <div className={"btn-group-vertical"}>
                        <button className={"optionsBtn"}>
                            <i className="fas fa-share"></i>
                            <span>   </span>Share
                        </button>
                        <button className={"optionsBtn"}>
                            <i className="fas fa-heart"></i>
                            <span>   </span>Save
                        </button>
                        <button className={"optionsBtn"}>
                            <i className="fas fa-map-marker-alt"></i>
                            <span>   </span>Show on Map
                        </button>
                    </div>
                </span>
                <div className="card-img-overlay">
                   {props.saleType}
                </div>
                <div className="card-body">
                    <div className={"estateAddress"}>
                        {props.name}
                    </div>
                    <div className={"estateConfig"}>
                        <i className={"fas fa-bed special"}></i>{props.config.beds}{"  "}
                        <i className={"fas fa-bath"}></i>{props.config.bath}{"  "}
                        <i className={"fas fa-ruler-combined"}></i>{props.config.area}
                    </div>
                    <div className={"estatePrice"}>
                        <div className={"currencyType"}></div>
                        {props.price}
                    </div>
                </div>
            </div>
    )
}

export default EstateCardView