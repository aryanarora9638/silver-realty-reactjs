import React, {useState} from "react"
import "./SearchEstateFilter.css"

import Button from "react-bootstrap/Button"
import Dropdown from "react-bootstrap/Dropdown"
import DropdownButton from "react-bootstrap/DropdownButton"

let SearchEstateFilter = (props) => {

    let filterList = []
    let [estateType, setEstateType] = useState("")
    let [estatePrice, setEstatePrice] = useState(0)
    let [estateBed, setEstateBed] = useState(0)
    let [estateBath, setEstateBath] = useState(0)
    let [estateArea, setEstateArea] = useState(0)
    filterList.push(
        <div key={"filters"}>
            {(estateType !== "") ? <Button>Type : {estateType}</Button>  : ""}
            {(estatePrice !== 0) ? <Button>Price : {estatePrice}</Button> : ""}
            {(estateBed !== 0)   ? <Button>Beds# : {estateBed}</Button>   : ""}
            {(estateBath !== 0)  ? <Button>Baths# : {estateBath}</Button>  : ""}
            {(estateArea !== 0)  ? <Button>Area : {estateArea}</Button>  : ""}
        </div>
    )

    let resetAllFilters = () => {
        setEstateType("")
        setEstatePrice(0)
        setEstateBed(0)
        setEstateBath(0)
        setEstateArea(0)

        props.removeFilter()
    }

    let applyFilters = async () => {
        let estateList = props.data

        if(estateType !== ""){
            estateList = await estateList.map(item  => {
                //Type
                let saleType = item["saleType"].substring(0, item["saleType"].toString().indexOf(" "))
                if (saleType.toLowerCase().trim() === estateType.toLowerCase().trim()) {
                    return item
                }
            }).filter(Boolean)
        }

        if(estatePrice !== 0){
            estateList = await estateList.map(item => {
                //Price
                let price = parseInt(item.estatePrice.split("").map(val => {
                    if(!Number.isNaN(parseInt(val))){
                        return val
                    }
                }).join(""))

                if(estatePrice === 500000 && price <= 500000){
                    return item
                }
               if(estatePrice === 1000000 && price >= 500000 && price <= 1000000){
                   return item
               }
                if(estatePrice === 2000000 && price >= 1000000 && price <= 2000000){
                    return item
                }
                if(estatePrice === 3000000 && price >= 2000000 && price <= 3000000){
                    return item
                }
                if(estatePrice === 4000000 && price >= 3000000 && price <= 4000000){
                    return item
                }
                if(estatePrice === 5000000 && price >= 4000000 && price <= 5000000){
                    return item
                }
                if(estatePrice === 6000000 && price >= 5000000){
                    return item
                }
            }).filter(Boolean)
        }

        if(estateBed !== 0){
            estateList = await estateList.map(item => {
                //Bed
                let beds = parseInt(item.estateConfig.beds)
                if(estateBed === 5 && beds > 5){
                    return item
                }
                if(estateBed === beds){
                    return item
                }
            }).filter(Boolean)
        }

        if(estateBath !== 0){
            estateList = await estateList.map(item => {
                //Bed
                let baths = parseInt(item.estateConfig.bath)
                if(estateBath === 5 && baths > 5){
                    return item
                }
                if(estateBath === baths){
                    return item
                }
            }).filter(Boolean)
        }

        if(estateArea !== 0){
            estateList = await estateList.map(item => {
                //Area
                let area = parseInt(item.estateConfig.area.split("").map(val => {
                    if(!Number.isNaN(parseInt(val))){
                        return val
                    }
                }).join(""))

                if(estateArea === 500 && area <= 500){
                    return item
                }
                if(estateArea === 1000 && area >= 500 && area <= 1000){
                    return item
                }
                if(estateArea === 2000 && area >= 1000 && area <= 2000){
                    return item
                }
                if(estateArea === 3000 && area >= 2000 && area <= 3000){
                    return item
                }
                if(estateArea === 4000 && area >= 3000 && area <= 4000){
                    return item
                }
                if(estateArea === 5000 && area >= 4000 && area <= 5000){
                    return item
                }
                if(estateArea === 6000 && area >= 5000){
                    return item
                }
            }).filter(Boolean)
        }

        //Filters out any unwanted values - here null and undefined in estateList array
        props.filterHandler(estateList)
    }

    return (
        <div className={"estate-filter"}>

            <div className={"filters"}>

                Filters :

                <DropdownButton id="dropdown-item-button" title="Type">
                    <Dropdown.Item as="button" onSelect={() => setEstateType("House")}>House</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateType("Condo")}>Condo</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateType("Studio")}>Studio</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateType("Townhouse")}>Townhouse</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateType("Apartment")}>Apartment</Dropdown.Item>
                </DropdownButton>

                <DropdownButton id="dropdown-item-button" title="Price">
                    <Dropdown.Item as="button" onSelect={() => setEstatePrice(500000)}>0-500,000</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstatePrice(1000000)}>500,000-1 Million</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstatePrice(2000000)}>1 Million-2 Million</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstatePrice(3000000)}>2 Million-3 Million</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstatePrice(4000000)}>3 Million-4 Million</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstatePrice(5000000)}>4 Million-5 Million</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstatePrice(6000000)}>5 Million+</Dropdown.Item>
                </DropdownButton>


                <DropdownButton id="dropdown-item-button" title="Beds">
                    <Dropdown.Item as="button" onSelect={() => setEstateBed(1)}>1</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateBed(2)}>2</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateBed(3)}>3</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateBed(4)}>4</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateBed(5)}>5+</Dropdown.Item>
                </DropdownButton>

                <DropdownButton id="dropdown-item-button" title="Baths">
                    <Dropdown.Item as="button" onSelect={() => setEstateBath(1)}>1</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateBath(2)}>2</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateBath(3)}>3</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateBath(4)}>4</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateBath(6)}>5+</Dropdown.Item>
                </DropdownButton>

                <DropdownButton id="dropdown-item-button" title="Area(sqft)">
                    <Dropdown.Item as="button" onSelect={() => setEstateArea(500)}>0-500</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateArea(1000)}>500-1000</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateArea(2000)}>1000-2000</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateArea(3000)}>2000-3000</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateArea(4000)}>3000-4000</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateArea(5000)}>4000-5000</Dropdown.Item>
                    <Dropdown.Item as="button" onSelect={() => setEstateArea(6000)}>5000+</Dropdown.Item>
                </DropdownButton>

                <Button onClick={() => applyFilters()}>
                    Update Search
                    <i className="fas fa-search"></i>
                </Button>

                <Button onClick={() => resetAllFilters()}>
                    Reset
                    <i className="fas fa-undo"></i>
                </Button>
            </div>


            <div className={"filter-applied-list"}>
                {filterList}
            </div>
        </div>
    )
}

export default SearchEstateFilter