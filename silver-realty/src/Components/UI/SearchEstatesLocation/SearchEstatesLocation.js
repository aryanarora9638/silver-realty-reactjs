import React from "react"
import "./SearchEstatesLocation.css"

import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';



let SearchEstateLocation = (props) => {

    let markers = []
    let center = {}

    if(props.mainCards.length !== 0){
        center = {lat : props.mainCards[0].estateLocation.latitude, lng : props.mainCards[0].estateLocation.longitude}
    }
    else {
        center = {lat : null, lng : null}
    }
    console.log(center)

    if(props.mainCards.length !== 0 && props.filteredCards.length === 0){
        props.mainCards.map((item,index) => {
            markers.push(
                <Marker position={{lat : item.estateLocation.latitude, lng: item.estateLocation.longitude}} key={index}/>
            )
        })
    }
    if(props.filteredCards.length !== 0){
        markers = []
        props.filteredCards.map((item,index) => {
            markers.push(
                <Marker position={{lat : item.props.location.latitude, lng: item.props.location.longitude}} key={index}/>
            )
        })
    }

    let mapStyles = {
        position: "relative",
        width: "100%",
        height: "300px"
    };

    return (
        <div className={"estate-location"}>
            <h5 className={"title"}>Map</h5>
            <div className={"map"}>
                {(center.lat === null && center.lng === null) ?  "" :
                    <Map
                    google={props.google}
                    zoom={13}
                    style={mapStyles}
                    initialCenter={{ lat: center.lat, lng: center.lng}}>
                    {markers}</Map>
                }
            </div>
        </div>
    )
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyAp7C8deBIZqAwQvcXh3MSNGsvZdPxHxNI'
})(SearchEstateLocation);