import React from "react"
import "./EstateConfigP.css"


let EstateConfigP = (props) => {

    //Address
    let _estateAddress = props.data.estateName
    let startIndex = _estateAddress.indexOf(",")
    let unit = _estateAddress.slice(0, startIndex).trim()
    let city = _estateAddress.slice(startIndex+1, _estateAddress.length).trim()

    let address = (
        <div className={"estateName"}>
            <div className={"unit"}>
                {unit}
            </div>
            <div className={"city"}>
                {city}
            </div>
        </div>
    )

    //Config
    let _config = props.data.estateConfig
    let beds = _config.beds
    let bath = _config.bath
    let area = _config.area

    let config = (
        <div className={"estateConfig"}>
            <div className={"config-type beds"}>
                <i className="fas fa-bed">
                    <span className={"config-text"}>{beds}</span>
                </i>
            </div>
            <div className={"config-type bath"}>
                <i className="fas fa-bath">
                    <span className={"config-text"}>{bath}</span>
                </i>
            </div>
            <div className={"config-type area"}>
                <i className="fas fa-ruler-combined">
                    <span className={"config-text"}>{area}</span>
                </i>
            </div>
        </div>
    )


    return (
        <div className={"estate-name-config"}>
            {address}
            {config}
        </div>
    )
}

export default EstateConfigP