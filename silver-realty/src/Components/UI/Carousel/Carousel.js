import React, {useState} from "react"
import "./Carousel.css"

import Carousel from "react-bootstrap/Carousel"


let BootstrapCarousel = (props) => {

    const [index, setIndex] = useState(0);
    const [sideImgIndex, setSideImgIndex] = useState(0)
    const [direction, setDirection] = useState(null);

    const handleSelect = (selectedIndex, e) => {
        setIndex(selectedIndex);
        setDirection(e.direction);
        if(index === props.images.length){
            setSideImgIndex(index-1)
        }
        else if(index === 0){
            setSideImgIndex(index+1)
        }
        else {
            setSideImgIndex(index)
        }
    };

    let carouselBody = props.images.map((imageUrl => {
         return (
                <Carousel.Item key={imageUrl}>
                    <img
                        className="d-block w-100"
                        src={imageUrl}
                        alt="Estate"
                     />
                    <Carousel.Caption>
                        <h5 className={"carousel-caption"}>{props.images.indexOf(imageUrl)+1} / {props.images.length+1}</h5>
                    </Carousel.Caption>
                </Carousel.Item>
         )
    }))



    return (
        <div className={"estate-gallery"}>
            <div className={"side-image-container"}>
                <img
                    className={"side-image"}
                    src={props.images[sideImgIndex]}
                    alt={""}/>
            </div>

            <Carousel
                onSelect={handleSelect}
                activeIndex={index}
                direction={direction}
                interval={30000}
                pauseOnHover={true}>
                {carouselBody}
            </Carousel>

            <div className={"side-image-container"}>
                <img
                    className={"side-image"}
                    src={props.images[sideImgIndex+1]}
                    alt={""}/>
            </div>
        </div>
    )
}

export default BootstrapCarousel