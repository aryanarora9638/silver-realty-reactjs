import React, {useState} from "react"
import "./EstateFacts.css"

let EstateFacts = (props) => {

    let mainFacts = []
    for(let key in props.data){
        let value = props.data[key]
        mainFacts.push(
            <div className={"fact"} key={key}>
                <div className={"key"}>
                    <h5>{key}</h5>
                </div>
                <div className={"value"}>{value}</div>
            </div>
        )
    }

    let moreFacts = []
    for(let key in props.subData){
        let values = []
        let value = props.subData[key]
        for(let subKey in value){
            let subValue = value[subKey]
            for(let subsKey in subValue){
                let subsValue = subValue[subsKey]
                values.push(
                    <div className={"value"} key={subsKey}>
                        <div className={"subKey"}>{subsKey}:</div>
                        <div className={"subValue"}> {subsValue} </div>
                    </div>
                )
            }
        }
        moreFacts.push(
            <div className={"fact"} key={key}>
                <div className={"key"}>
                    <h5>{key}</h5>
                </div>
                <div className={"values"}>{values}</div>
            </div>
        )
    }

    let [showBtnText, setShowBtnText] = useState( "Show more...")
    let showMoreFactsHandler = () => {
        let el = document.getElementById("more-facts")
        if(el.style.display !== "none"){
            el.style.display = "none"
            setShowBtnText("Show more...")
        }
        else {
            el.style.display = "inline-flex"
            setShowBtnText("Show less...")
        }
    }

    return (
        <div className={"estate-facts"}>
            <h5 className={"title"}>Facts</h5>
            <div className={"main-facts"}>
                {mainFacts}
            </div>
            <div className={"show-more-facts"}>
                <button onClick={() => showMoreFactsHandler()}>{showBtnText}</button>
            </div>
            <div className={"more-facts active"} id={"more-facts"}>
                <div className={"facts"}>
                    {moreFacts}
                </div>
            </div>
        </div>
    )
}

export default EstateFacts