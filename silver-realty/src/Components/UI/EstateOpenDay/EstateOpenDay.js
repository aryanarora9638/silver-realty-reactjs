import React from "react"
import "./EstateOpenDay.css"

import Card from "react-bootstrap/Card"

let EstateOpenDay = (props) => {

    let openDayDetails = []
    let numOpenDays = 0
    if(Object.keys(props.data).length >= 1){
        numOpenDays = Object.keys(props.data).length
        for(let key in props.data){
            let obj = props.data[key]
            openDayDetails.push(
                <div className={"open-day-details"} key={obj.day}>
                    <div className={"day"}>{obj.day}</div>
                    <div className={"time"}>{obj.time}</div>
                </div>
            )
        }
    }
    else {
        numOpenDays = 0
        openDayDetails.push(
            <div className={"open-day-details"} key={"open-day-unavailable"}>
                <div className={"open-day-unavailable"}>
                    No Open Day Available, Check back later
                </div>
            </div>
        )
    }

    return (
        <Card>
            <Card.Body>
                <Card.Title>
                    Open Day
                    <div className={"num-open-days"}>
                        {(numOpenDays > 0) ? numOpenDays : "!"}
                    </div>

                </Card.Title>
                {openDayDetails}
            </Card.Body>
        </Card>
    )
}

export default EstateOpenDay