import React, {useState} from "react"
import "./EstateOverview.css"

import Card from "react-bootstrap/Card"

import EstateDescription from "../EstateDescription/EstateDescription";


let EstateOverview = (props) => {

    let [estateSaves, setEstateSaves] = useState(props.data.Saves)
    let estateSaveHandler = () => {
        let el =  document.getElementById("saves").classList
        if(el.contains("red")){
            setEstateSaves(parseInt(estateSaves) - 1)
            el.remove("red")
        }
        else {
            setEstateSaves(parseInt(estateSaves) + 1)
            el.add("red")
        }
    }
    let estateGeneralStanding = (
        <div className={"estate-general-standing"}>
            <Card
                className={"card saves"}>
                <Card.Body>
                    <Card.Title>Saves <i className="far fa-heart" id={"saves"} onClick={() => estateSaveHandler()}></i> </Card.Title>
                    <Card.Subtitle>{estateSaves}</Card.Subtitle>
                </Card.Body>
            </Card>
            <Card
                className={"card time"}>
                <Card.Body>
                    <Card.Title>Time on Silver Realty <i className="far fa-clock"></i></Card.Title>
                    <Card.Subtitle>{props.data["Time on Zillow"]}</Card.Subtitle>
                </Card.Body>
            </Card>
            <Card
                className={"card views"}>
                <Card.Body>
                    <Card.Title>Views <i className="far fa-eye"></i></Card.Title>
                    <Card.Subtitle>{props.data.Views}</Card.Subtitle>
                </Card.Body>
            </Card>
        </div>
    )


    return (
        <div className={"estate-overview"}>
            {estateGeneralStanding}
            {console.log(props.data.Saves, props.data["Time on Zillow"], props.data.Views)}
            <EstateDescription data={props.data.discription} agentData={props.data.agentInfo} openDayData={props.data.openDay}/>
        </div>
    )
}

export default EstateOverview