import React, {useState} from "react"
import "./EstatePageView.css"

import Modal from "react-bootstrap/Modal"

import BootstrapCarousel from "../../Carousel/Carousel"
import BtnLoading from "../../Spinner/BtnLoading/BtnLoading"
import EstateConfg from "../../EstateConfig/PageView/EstateConfigP"


let EstatePageView = (props) => {
    let [modalShow, setShow] = useState(props.test)
    console.log(modalShow)



    let body = (
        <React.Fragment>
            {/*Top Carousel*/}
            {(props.estateData) ? <BootstrapCarousel images={props.estateData}/> : "Carousel"}
            <hr/>
            {(props.estateData) ? <EstateConfg data={props.estateData.coverInfo}/> : "Estate config"}





        </React.Fragment>
    )

    return (
            <Modal
                show={props.test}
                onHide={() => {
                    setShow(false)
                    props.testModalClickHandler()}}
                dialogClassName="modal-90w"
                aria-labelledby="estatePageViewModal"
                centered >

                <Modal.Header closeButton>
                    <Modal.Title id="estatePageViewModal">
                        Custom Modal Styling
                    </Modal.Title>
                </Modal.Header>

                <Modal.Body>

                    {(props.estateDataLoading) ? <BtnLoading/> :  body}

                </Modal.Body>

            </Modal>
    )
}

export default EstatePageView