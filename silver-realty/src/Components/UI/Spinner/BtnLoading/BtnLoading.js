import React from "react"
import "./BtnLoading.css"

let BtnLoading = (props) => {
    return (
        <button className="btn btn-primary" disabled>
            <span className="spinner-border spinner-border-sm"></span>
            Loading Data..
        </button>
    )
}

export default BtnLoading