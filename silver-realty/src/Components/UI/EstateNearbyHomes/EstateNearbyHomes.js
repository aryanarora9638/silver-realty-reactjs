import React from "react"
import "./EstateNearbyHomes.css"

let EstateNearbyHomes = (props) => {

    console.log(props.data)

    let nearbyHomes
    if(Object.keys(props.data).length === 0){
        nearbyHomes = (
            <div>
                No result found...
            </div>
        )
    }

    return (
        <div className={"estate-near-by-homes"}>
            {/*Skipping coz zillow.com website nearby homes error*/}
            <h5 className={"title"}>Near by Homes</h5>
            {nearbyHomes}
        </div>
    )
}

export default EstateNearbyHomes