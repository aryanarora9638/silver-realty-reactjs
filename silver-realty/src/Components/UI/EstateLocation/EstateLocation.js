import React from "react"
import "./EstateLocation.css"

import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';

let EstateLocation = (props) => {
    let neighborhoodScores = []
    for (let key in props.subData) {
        let obj = props.subData[key]
        neighborhoodScores.push (
            <div className={"score"} key={obj.type}>
                <div className={"type"}>
                    {(obj.type === " Walk Score ®") ? <i className="fas fa-walking"> {obj.type} </i> : <i className="fas fa-bus"> {obj.type} </i>}
                </div>
                <div className={"score"}>{obj.score}</div>
                <div className={"description"}>{obj.description}</div>
            </div>
        )
    }


    let center = {
        lat : props.data.latitude,
        lng : props.data.longitude
    }

    let mapStyles = {
        position: "relative",
        width: "100%",
        height: "300px"
    };

    return (
        <div className={"estate-location"}>
            <h5 className={"title"}>
                Location
            </h5>
            <div className={"neighborhood-scores"}>
                {neighborhoodScores}
            </div>
            <div className={"map"}>
                <Map
                    google={props.google}
                    zoom={13}
                    style={mapStyles}
                    initialCenter={{ lat: center.lat, lng: center.lng}}>

                    <Marker position={{ lat: center.lat, lng: center.lng}} />
                </Map>
            </div>
        </div>
    )
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyAp7C8deBIZqAwQvcXh3MSNGsvZdPxHxNI'
})(EstateLocation);