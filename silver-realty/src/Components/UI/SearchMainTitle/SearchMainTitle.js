import React, {useState} from "react"
import "./SearchMainTitle.css"

import Button from "react-bootstrap/Button"

let SearchMainTitle = (props) => {

    let [city, setCity] = useState(props.city.charAt(0).toUpperCase() + props.city.substr(1, props.city.length) + ((props.province === 'rb') ? "" : (", " + props.province.toUpperCase())))

    let inputHandler = () => {
       setCity(
           document.getElementById("city-province-input").value
       )
    }


    return (
        <div className={"search-main-title"}>
            <div className={"image"}>
                <img src={require("../../../assets/searchMainTitle/img/2.jpg")} alt="title"/>
            </div>
            <div className={"title"}>
                <h2>Find you way home worldwide 👇</h2>
                <div className={"subtitle"}>
                    <h4>No matter where you are - you'd like to spend less time and money. With Silver Realty search the home of our dreams. We bring the best in class estate right to your finger tips.</h4>
                </div>
                <div className={"city-province"}>
                    <input
                        id={"city-province-input"}
                        type="text"
                        name={"city-province-input"}
                        placeholder={"Type you city, province here"}
                        onChange={inputHandler}
                        value={city}/>
                    <Button onClick={() => props.handleChange()}>
                        <i className="fas fa-search"></i>
                    </Button>
                </div>
            </div>
        </div>
    )
}

export default SearchMainTitle