import React, {useState} from "react"
import "./EstatePrice.css"

import Card from "react-bootstrap/Card"
import Button from "react-bootstrap/Button"
import ListGroup from 'react-bootstrap/ListGroup'

let EstatePrice = (props) => {

    let priceCommaHandler = (oldPrice) => {
        let counter = 0
        return oldPrice.toString().split("").map(item => {
            counter += 1
            if(counter % 3 === 2 && counter !== 1 && counter !== oldPrice.toString().length){
                return ("," + item)
            }
            return item
        })
    }

    //Current Price and Event
    let [currentPrice, setCurrentPrice] = useState(() => {
        let price  = null
        for(let key in props.data){
            let obj = props.data[key]
            if(obj.event === "Listed for sale"){
                price = obj.price
                price = price.split("").map(key => {
                    if(!isNaN(key)){
                        return key
                    }
                })
                price = Math.round(parseInt(price.join("")))
            }
        }
        return price
    })

    let [currencyType, setCurrencyType] = useState("CAD")

    let currencyUnitHandler = (type) => {
        let price
        if(type === "USD" && currencyType === "CAD"){
            price = Math.round(currentPrice/1.33)
            setCurrentPrice(price)
            setCurrencyType("USD")

            document.getElementById("usdBtn").classList.add("active")
            document.getElementById("cadBtn").classList.remove("active")
        }

        if(type === "CAD" && currencyType === "USD"){
            price = Math.round(currentPrice*1.33)
            setCurrentPrice(price)
            setCurrencyType("CAD")

            document.getElementById("cadBtn").classList.add("active")
            document.getElementById("usdBtn").classList.remove("active")
        }
    }

    //Price History and Events
    let priceHistory = []
    for(let key in props.data){
        let obj = props.data[key]
        priceHistory.push(
            <ListGroup.Item key={obj.date}>
                <div className={"price-events event"}>{obj.event}</div>
                <div className={"price-events date"}>{obj.date}</div>
                <div className={"price-events price"}>{obj.price}</div>
            </ListGroup.Item>
        )
    }

    return (
        <div className={"estate-price"}>
            <Card>
                <Card.Body>
                    <Card.Title>Price History</Card.Title>
                    <Card.Subtitle>
                        <Button onClick={() => currencyUnitHandler("CAD")} active id={"cadBtn"}>CAD</Button>
                        <Button onClick={() => currencyUnitHandler("USD")} id={"usdBtn"}>USD</Button>
                    </Card.Subtitle>
                    <Card.Text>
                        {currencyType} {priceCommaHandler(currentPrice)}
                    </Card.Text>
                </Card.Body>
                <ListGroup className="list-group-flush">
                    {priceHistory}
                </ListGroup>
            </Card>
        </div>
    )
}

export default EstatePrice