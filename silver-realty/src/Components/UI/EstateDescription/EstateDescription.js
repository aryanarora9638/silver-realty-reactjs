import React from "react"
import "./EstateDescription.css"

import EstateListingAgent from "../EstateListingAgent/EstateListingAgent";
import EstateOpenDay from "../EstateOpenDay/EstateOpenDay";


let EstateDescription = (props) => {
    return (
        <div className={"estate-description"}>
            <h5 className={"title"}>Description</h5>
            <div className={"body"}>
                {props.data}
            </div>
            <div className={"estate-open-day"}>
                <EstateOpenDay data={props.openDayData}/>
            </div>
            <div className={"estate-listing-agent"}>
                <EstateListingAgent data={props.agentData}/>
            </div>
        </div>
    )
}

export default EstateDescription