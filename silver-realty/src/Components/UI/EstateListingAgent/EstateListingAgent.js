import React from "react"
import "./EstateListingAgent.css"

import Card from "react-bootstrap/Card"

let EstateListingAgent = (props) => {
    return (
        <Card>
            <Card.Body>
                <Card.Title>
                    <div className={"salesperson-name"}>{props.data.name}</div>
                    <div className={"company-image"}>
                        <img src={props.data.companyImageLink} alt="company"/>
                    </div>
                    <div className={"company-name"}>{props.data.companyName}</div>
                </Card.Title>
                {/*<Card.Subtitle>*/}
                {/*    <div className={"company-image"}>*/}
                {/*        <img src={props.data.imageLink} alt="salesperson"/>*/}
                {/*    </div>*/}
                {/*    <div className={"name"}>{props.data.name.toString()}</div>*/}
                {/*</Card.Subtitle>*/}
            </Card.Body>

        </Card>
    )
}

export default EstateListingAgent
