import React, {useState} from "react"
import "./EstatesHolder.css"

import Button from "react-bootstrap/Button"
import EstateCardView from "../Card/EstateCardView/EstateCardView";
import SearchEstateFilter from "../SearchEstateFilter/SearchEstateFilter";
import SearchEstateLocation from "../SearchEstatesLocation/SearchEstatesLocation";

let EstatesHolder = (props) => {

    let cardsTest
    let [cardFiltered, setCardFiltered] = useState([])
    if(props.cardList !== null){
        cardsTest = props.cardList.map(card => {
            let saleType = card["saleType"].substring(0,card["saleType"].toString().indexOf(" "))
            let estateLocation = card["estateLocation"]
            return (
                <EstateCardView
                    key={card["estateName"]}
                    name={card["estateName"]}
                    imageLink={card["estateCoverImgLink"]}
                    link={card["estateLink"]}
                    config={card["estateConfig"]}
                    price={card["estatePrice"]}
                    saleType={saleType}
                    location={estateLocation}
                    estateClick={(estateLink, location) => props.cardClickHandler(estateLink, estateLocation)}
                />
            )
        })
    }

    let applyFilters = async (cards) => {
        let newList = await cards.map(card => {
            let saleType = card["saleType"].substring(0,card["saleType"].toString().indexOf(" "))
            let estateLocation = card["estateLocation"]
            return (
                <EstateCardView
                    key={card["estateName"]}
                    name={card["estateName"]}
                    imageLink={card["estateCoverImgLink"]}
                    link={card["estateLink"]}
                    config={card["estateConfig"]}
                    price={card["estatePrice"]}
                    saleType={saleType}
                    location={estateLocation}
                    estateClick={(estateLink, location) => props.cardClickHandler(estateLink, estateLocation)}
                />
            )
        })
        setCardFiltered(newList)
    }

    let removeFilter = () => {
        setCardFiltered([])
    }

    return (
        <div className={"estates-holder"}>
            <div className={"title"}>
                <h3>Get a choice of your housing</h3>
            </div>
            <div className={"city-province-viewer"}>
                <Button>
                    <i className="fas fa-map-marker"></i>
                    {props.city.charAt(0).toUpperCase() + props.city.substr(1, props.city.length)}
                    {(props.province === 'rb') ? "" : (", " + props.province.toUpperCase())}
                </Button>
            </div>
            <div className={"subtitle"}>
                <h5>Build ease the process of viewing the property, weather you are a host or the home owner</h5>
            </div>
            <SearchEstateFilter data={props.cardList} filterHandler={(cards) => applyFilters(cards)} removeFilter={() => removeFilter()}/>
            <div className={"estate-cards"}>
                <div className={"cards"}>
                    {(cardFiltered.length === 0) ? cardsTest : cardFiltered}
                    {(cardsTest.length === 0) ? "Loading..." : ""}
                </div>
            </div>
           <SearchEstateLocation mainCards={props.cardList} filteredCards={cardFiltered}/>
        </div>
    )
}

export default EstatesHolder