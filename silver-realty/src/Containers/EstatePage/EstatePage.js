import React from "react"
import "./EstatePage.css"

import axios from "axios"
import {withRouter} from "react-router-dom"

import BootstrapCarousel from "../../Components/UI/Carousel/Carousel";
import EstatePrice from "../../Components/UI/EstatePrice/EstatePrice";
import EstateFacts from "../../Components/UI/EstateFacts/EstateFacts";
import EstateLocation from "../../Components/UI/EstateLocation/EstateLocation";
import EstateOverview from "../../Components/UI/EstateOverview/EstateOverview";
import EstateConfigP from "../../Components/UI/EstateConfig/PageView/EstateConfigP";
import EstateNearbyHomes from "../../Components/UI/EstateNearbyHomes/EstateNearbyHomes";

class EstatePage extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
            selectedEstateLink : null,
            estateData : null,
            estateLocation : null,
            dataLoading : true
        }
    }

    componentDidMount() {

        let estateLink
        (this.props.location.state) ? estateLink = this.props.location.state.estateLink : estateLink = "No Result"

        if (estateLink === "No Result") {
            this.setState({
                selectedEstateLink: null,
                estateLocation : null
            })
        } else {
            this.setState({
                selectedEstateLink: estateLink,
                estateLocation : this.props.location.state.estateLocation
            })
            axios
                .get("//localhost:3001/estates/" + estateLink)
                .then(data => {
                    console.log(data.data)
                    this.setState({
                        estateData : data.data,
                        dataLoading : false
                    })
                })
        }
    }

    setImageView = () => {
        return (
            <div className={"estate-images"}>
                <BootstrapCarousel images={this.state.estateData.gallery.imageLink}/>
            </div>
        )
    }

    render() {

        let result
        if(!this.state.dataLoading){
            //All data has been loaded
            let imageGallery = this.setImageView()

            result = (
                <div>
                    <div>
                        {this.state.selectedEstateLink}
                    </div>

                    {imageGallery}
                    <EstateConfigP data={this.state.estateData.coverInfo}/>
                    <EstatePrice data={this.state.estateData.priceHistory}/>
                    <EstateOverview data={this.state.estateData.estateOverView}/>
                    <hr className={"horizontal-ruler"}/>
                    <EstateFacts data={this.state.estateData.factsAndFeatures} subData={this.state.estateData.moreFacts}/>
                    <hr className={"horizontal-ruler"}/>
                    <EstateNearbyHomes data={this.state.estateData.nearbyHome}/>
                    <hr className={"horizontal-ruler"}/>
                    <EstateLocation data={this.state.estateLocation} subData={this.state.estateData.neighborhoodScores}/>
                    <hr className={"horizontal-ruler"}/>
                </div>
            )
        }
        else {
            result = (
                <div className={"loading-screen"}> Loading... </div>
            )
        }


        return(
            <div>
                {result}
            </div>
        )
    }
}

export default withRouter(EstatePage)