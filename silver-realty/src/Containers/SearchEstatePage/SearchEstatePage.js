import React from "react"
import "./SearchEstatePage.css"
import {Redirect} from "react-router-dom"

import axios from "axios"

import EstatesHolder from "../../Components/UI/EstatesHolder/EstatesHolder";
import SearchMainTitle from "../../Components/UI/SearchMainTitle/SearchMainTitle";


class SearchEstatePage extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            estateCards : [],
            selectedEstateLink : null,
            selectedEstate : null,
            selectedEstateLocation : null,
            selectedEstateLoading : false,
            showModal : false,
            city : "vancouver",
            province : "bc"
        }
    }

    componentDidMount() {
        let extractedData = []
        axios
            .get("//localhost:3001/estates/" + this.state.city + "/" + this.state.province)
            .then(data => {
                extractedData = data.data
                this.setState({
                    estateCards : extractedData
                })
            })
    }

    updateEstateLink = (link) => {
        let startIndex = link.indexOf("homedetails") + ("homedetails".length + 1)
        let endIndex = link.indexOf("/", startIndex)
        let idStartIndex = link.indexOf("/", endIndex)
        let idEndIndex = link.indexOf("/", idStartIndex+1)
        let secretKey = "[aryanSecretKey]"
        return (link.substring(startIndex,endIndex) + secretKey + link.substring(idStartIndex+1, idEndIndex))
    }

    estateClickHandler = async (estateLink, estateLocation) => {
        console.log("estate clicked with estate Link - ", estateLink)

        let updatedLink = this.updateEstateLink(estateLink)
        await this.setState({
            selectedEstateLink : updatedLink,
            selectedEstateLocation : estateLocation,
            selectedEstateLoading : true
        })
    }

    handleCityProvinceChange = async () => {
        let newInput = document.getElementById("city-province-input").value
        let newCity
        let newProvince
        if(newInput.includes(",")){
            newCity = newInput.substring(0, newInput.indexOf(",")).trim()
            newProvince = newInput.substring(newInput.indexOf(",") + 1, newInput.length).trim()
        }
        else {
            newCity = newInput.toString().trim()
            newProvince = "rb"
        }
        await this.setState({
            city : newCity,
            province : newProvince,
            estateCards : []
        })

        let extractedData = []
        await axios
            .get("//localhost:3001/estates/" + this.state.city + "/" + this.state.province)
            .then(data => {
                extractedData = data.data
                this.setState({
                    estateCards : extractedData
                })
            })
    }


    render() {

        let redirect
        if(this.state.selectedEstateLink && this.state.selectedEstateLocation){
            redirect = (
                <Redirect to={{
                    pathname : "/estatePage",
                    state : {
                        estateLink : this.state.selectedEstateLink,
                        estateLocation : this.state.selectedEstateLocation
                    }
                }}/>
            )
            this.setState({
                selectedEstateLink : null,
                selectedEstateLocation : null
            })
        }
        else {
            redirect = null
        }

        return (
            <div className={"search-estate-page"}>
                {redirect}
                <SearchMainTitle
                    city={this.state.city}
                    province={this.state.province}
                    handleChange={this.handleCityProvinceChange}/>
                <hr className={"horizontal-ruler"}/>
                <EstatesHolder
                    city={this.state.city}
                    province={this.state.province}
                    cardList={this.state.estateCards}
                    cardClickHandler={(estateLink, location) => this.estateClickHandler(estateLink, location)}/>
                <hr className={"horizontal-ruler"}/>
            </div>

        )
    }

}

export default SearchEstatePage