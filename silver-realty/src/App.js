import React from "react"
import "./App.css"

import {Link, Route, Switch} from "react-router-dom"

import EstatePage from "./Containers/EstatePage/EstatePage";
import SearchEstatePage from "./Containers/SearchEstatePage/SearchEstatePage";

let App = () => {
    return (
        <div>


            <Link to={"/"} exact={"true"}>Home Page</Link>
            <Link to={"/searchEstate"} exact={"true"}>Search Estate Page</Link>
            <Link to={"/estatePage"} exact={"true"}>Estate Page Testing</Link>


            <Switch>
                <Route
                    path={'/'}
                    exact={true}
                    render={() => <div> Home Page Welcomes</div>}/>

                <Route
                    path={'/searchEstate'}
                    exact={true}
                    component={SearchEstatePage}/>

                <Route
                    path={'/estatePage'}
                    exact={true}
                    component={EstatePage}/>

                <Route
                    render={() => <h1>Unknown Page : Error : 404</h1>}/>
            </Switch>

        </div>
    )
}

export default App